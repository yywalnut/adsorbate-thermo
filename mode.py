#!/usr/bin/env python

from ase.vibrations import Vibrations
import numpy as np


class ModeAtoms:
    """Wrapper around atoms to transform normal mode coordinates to Cartesian
    coordinates and back.

    Parameters
    ----------

    atoms : an ASE atoms object
        The ASE atoms object that is being transformed
    S : numpy matrix
        Optional transformation matrix. Will be calculated automatically
        from vibrations if not supplied.
    indices : list
        List of free atoms (that were used to make the vibrations if 
        vibname is supplied). Alternatively, mask can be supplied.
    mask : list
        Mask (e.g., [True, True, False, True, ...]) as alternate form
        to supply indices of free atoms.
    vibname : str
        Prefix from call to ASE's vibration module used when performing
        the normal-mode analysis.
    """

    def __init__(self, atoms, S=None, indices=None, mask=None,
                 vibname='vib'):
        self.atoms = atoms
        self.atoms_for_saving = self.atoms  # for trajectory writer
        self.constraints = []
        # Make self.info a reference to the underlying atoms' info dictionary.
        self.info = self.atoms.info

        if indices is None and mask is None:
            raise ValueError('Use "indices" or "mask".')
        if indices is not None and mask is not None:
            raise ValueError('Use only one of "indices" and "mask".')

        if mask is not None:
            self.index = np.asarray(mask, bool)
            self.n = self.index.sum()
        else:
            self.index = np.asarray(indices, int)
            self.n = len(self.index)

        self._len = len(atoms) # int, number of atoms in the atoms object
        if indices == None:
            indices = range(len(atoms))
        self._indices = indices # list of int, the vibration atom indices
        if S == None:
            '''.pckl files must provided in pwd'''
            S = np.empty((3*len(self._indices), 3*len(self._indices)))
            vib = Vibrations(atoms, indices=self._indices, name=vibname)
            vib.read()
            for i in range(3*len(indices)):
                s_row = []
                for j in indices:
                    s_row = s_row + vib.get_mode(i)[j].tolist()
                S[i] = np.asarray(s_row)
        self._S = S #S is the mode matrix, 3n*3n shape, n is len(indices); each row represents one mode
        self._item_position = atoms.positions.copy()

    def __len__(self):
        return self._len

    def get_c_positions(self):
        '''Return the Cartesian coordinates of the ModeAtoms object'''
        return self.atoms.get_positions()

    def get_position_vector(self):
        """Return normal mode coordinates as a vector; each element
        of the vector is a multiplier for the displacement of the
        corresponding eigenvector.
        """
        indices_position = []
        for j in self._indices:
            indices_position = (indices_position +
                                self.atoms.get_positions()[j].tolist())
        indices_position = np.asarray(indices_position)
        #indices_nm_position = (np.dot(self._S, indices_position)
        #                       .reshape(len(self._indices), 3))
        return np.dot(self._S, indices_position)

    def get_positions(self):
        """Return normal mode coordinates in same array size as the normal
        atoms.get_positions would return. Atoms not included in the indices
        list will be included, but as zeros.
        """
        vector = self.get_position_vector()
        indices_nm_position = vector.reshape(len(self._indices), 3)
        nm_position = np.zeros((self._len, 3))
        for j in self._indices:
            nm_position[j] = indices_nm_position[self._indices.index(j)]
        return nm_position

    def set_positions(self, positions):
        '''Given a normal mode coordinates (positions), set the atoms to the corresponding Cartesian
        positions: array of floats, 3n*1 shape'''
        indices_nm_position = []
        for j in self._indices:
            indices_nm_position = indices_nm_position + positions[j].tolist()
        indices_nm_position = np.asarray(indices_nm_position)
        indices_c_positions = np.dot(np.linalg.inv(self._S), indices_nm_position).reshape(len(self._indices), 3)


        c_positions = np.empty((self._len, 3))

        for i in range(self._len):
            if i not in self._indices:
                c_positions[i] = self._item_position[i]
            else:
                c_positions[i] = indices_c_positions[self._indices.index(i)]
        self.atoms.set_positions(c_positions)

    positions = property(fget=get_positions, fset=set_positions,
                         doc='Attribute for direct manipulation of the '
                         'transformed positions.')

    def get_potential_energy(self, **kwargs):
        return self.atoms.get_potential_energy(**kwargs)

    def get_forces(self, **kwargs):
        forces = self.atoms.get_forces(**kwargs)
        indices_force = []
        for j in self._indices:
            indices_force = indices_force + forces[j].tolist()
        indices_force = np.asarray(indices_force)
        indices_nm_forces = np.dot(self._S, indices_force).reshape(len(self._indices), 3)

        nm_forces = np.zeros((self._len, 3))
        for j in self._indices:
            nm_forces[j] = indices_nm_forces[self._indices.index(j)]
        return nm_forces

    def get_cell(self):
        """Returns the computational cell. The computational cell is the same as for the original system."""
        return self.atoms.get_cell()

    cell = property(fget=get_cell,
                    doc='Attribute to directly get unit cell.')

    def get_pbc(self):
        """Returns the periodic boundary conditions.The boundary conditions are the same as for the original system."""
        return self.atoms.get_pbc()

    pbc = property(fget=get_pbc,
                   doc='Attribute to directly get pbc.')

    def get_momenta(self):
        'Return the momenta of the visible atoms.'
        return self.atoms.get_momenta()[self.index]

    def get_initial_charges(self):
        'Return the initial charges of the visible atoms.'
        return self.atoms.get_initial_charges()[self.index]

    def set_momenta(self, momenta):
        'Set the momenta of the visible atoms.'
        mom = self.atoms.get_momenta()
        mom[self.index] = momenta
        self.atoms.set_momenta(mom)

    def get_atomic_numbers(self):
        'Return the atomic numbers of the visible atoms.'
        return self.atoms.get_atomic_numbers()[self.index]

    def set_atomic_numbers(self, atomic_numbers):
        'Set the atomic numbers of the visible atoms.'
        z = self.atoms.get_atomic_numbers()
        z[self.index] = atomic_numbers
        self.atoms.set_atomic_numbers(z)

    numbers = property(fget=get_atomic_numbers, fset=set_atomic_numbers,
                       doc="Don't use this.")

    def get_tags(self):
        'Return the tags of the visible atoms.'
        return self.atoms.get_tags()[self.index]

    def set_tags(self, tags):
        'Set the tags of the visible atoms.'
        tg = self.atoms.get_tags()
        tg[self.index] = tags
        self.atoms.set_tags(tg)

    def get_stress(self):
        return self.atoms.get_stress()

    def get_stresses(self):
        return self.atoms.get_stresses()[self.index]

    def get_masses(self):
        return self.atoms.get_masses()[self.index]

    def get_chemical_symbols(self):
        return self.atoms.get_chemical_symbols()

    def get_initial_magnetic_moments(self):
        return self.atoms.get_initial_magnetic_moments()

    def get_calculator(self):
        """Returns the calculator.WARNING: The calculator is unaware of this filter, and sees a
        different number of atoms."""
        return self.atoms.get_calculator()

    def get_celldisp(self):
        return self.atoms.get_celldisp()

    def has(self, name):
        'Check for existence of array.'
        return self.atoms.has(name)

    def __getitem__(self, i):
        'Return an atom.'
        return self.atoms[self.index[i]]
