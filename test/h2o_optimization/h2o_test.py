#!/usr/bin/env python

from ase import Atoms
from ase.vibrations import Vibrations
import numpy as np

class ModeAtoms:
    """Wrapper around atoms to transform normal mode coordinates to Cartesian coordinates and back."""

    def __init__(self, atoms, S=None, indices=None):
        self._atoms = atoms
        self._len = len(atoms) # int, number of atoms in the atoms object
        if indices == None:
            indices = range(len(atoms))
        self._indices = indices # list of int, the vibration atom indices
        if S == None: 
            '''.pckl files must provided in pwd'''
            S = np.empty((3*self._len, 3*self._len))
            vib = Vibrations(atoms, indices=self._indices)
            vib.read()
            for i in range(3*len(indices)):
                S[i] = vib.get_mode(i).ravel()
        self._S = S #S is the mode matrix, 3n*3n shape; each row represents one mode
        self._mean_position = atoms.positions.copy()
    
    def __len__(self):
        return len(self._atoms)
    
    def set_nm_positions(self): 
        '''set ModeAtoms object into normal mode coordinates'''
        positions = self.get_positions()
        self._atoms.set_positions(positions)
    
    def get_c_positions(self):
        '''Return the Cartesian coordinates of the ModeAtoms object'''
        return self._atoms.get_positions()
            
    def get_positions(self):
        '''return normal mode coordinates'''
        self._nm_position = np.dot(self._S, self._mean_position.ravel())
        return self._nm_position.reshape((self._len, 3))
    
    def set_positions(self, positions):
        '''Given a normal mode coordinates (positions), set the atoms to the corresponding Cartesian
        positions: array of floats, 3n*1 shape'''  
        c_positions = np.dot(np.linalg.inv(self._S), positions.ravel())
        self._atoms.set_positions(c_positions.reshape((self._len, 3)))
            
    def get_potential_energy(self, **kwargs):
        return self._atoms.get_potential_energy(**kwargs)
    
    def get_forces(self, **kwargs):
        forces = self._atoms.get_forces(**kwargs)
        mode_forces = np.dot(self._S, forces.ravel())
        return mode_forces.reshape((self._len, 3))
    
    def __len__(self):
        return len(self._atoms)
    
if __name__ == '__main__':
    from ase import io
    from ase.calculators.jacapo import Jacapo
    from ase.optimize import QuasiNewton
    from ase.visualize import view
    from ase.parallel import paropen
    import os
    
    calc = Jacapo(nc='out.nc', #nc output file
                  pw=340.15, #planewave cutoff
                  dw=500.00, #density cutoff
                  nbands=None, # number of bands
                  kpts=(1,1,1), # k points
                  xc='RPBE', #exchange correlation method
                  ft=0.01, #Fermi temperature
                  symmetry=False,
                  dipole={'status':True,
                          'mixpar':0.2,
                          'initval':0.0,},
                  ncoutput={'wf':'No',
                            'cd':'No',
                            'efp':'No',
                            'esp':'No'},
                  convergence={'energy':0.00001,
                               'density':0.0001,
                               'occupation':0.001,
                               'maxsteps':None,
                               'maxtime':None},
                  spinpol=False)
    
    pwd = os.getcwd()
    
    os.chdir('./h2o_example/')
    atoms = io.read('./qn.traj', index=-1)
    atoms.set_calculator(calc)
    modeatoms = ModeAtoms(atoms=atoms)
    
    os.chdir(pwd)
    dyn = QuasiNewton(modeatoms, logfile='qn_nm.log', trajectory=None)
    dyn.run(fmax=0.005) #what's the fmax?
    
    energy = modeatoms.get_potential_energy()
    f = paropen('out.energy','w')
    f.write(str(energy))
    f.close()
