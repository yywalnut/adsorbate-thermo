#! /usr/bin/env python

from ase import io
from ase.vibrations import Vibrations
from ase.parallel import paropen
from ase.thermochemistry import IdealGasThermo
from ase.visualize import view
from ase import Atoms


#set up calculator
atoms = io.read('./qn.traj',index=-1)
atoms.set_pbc((1.,1.,1.))

#vibration mode
vib = Vibrations(atoms)
vib.read()
#hnu = vib.get_energies()
#print 0.5*hnu.real.sum()??
for i in range(9):
    print vib.get_mode(i)
