#! /usr/bin/env python

from ase import io
from ase.data.molecules import molecule
from ase.vibrations import Vibrations
from ase.parallel import paropen
from ase.calculators.jacapo import *
from ase.thermochemistry import IdealGasThermo
from ase.optimize import QuasiNewton

#set up calculator
atoms = io.read('/users/yz144/data/Document/molecules_dacapo/h2o/qn.traj',
               index=-1)
atoms.set_pbc((1.,1.,1.))

calc = Jacapo(nc='out.nc', #nc output file
              pw=340.15, #planewave cutoff
              dw=500.00, #density cutoff
              nbands=10, # number of bands
              kpts=(1,1,1), # k points
              xc='RPBE', #exchange correlation method
              ft=0.01, #Fermi temperature
              symmetry=True,
              ncoutput={'wf':'No',
                        'cd':'No',
                        'efp':'No',
                        'esp':'No'},
              convergence={'energy':0.00001,
                           'density':0.0001,
                           'occupation':0.001,
                           'maxsteps':None,
                           'maxtime':None},
              spinpol=False,
             )
atoms.set_calculator(calc)

dyn=QuasiNewton(atoms, logfile = 'qn.log', trajectory = 'qn.traj')
dyn.run(fmax=0.001)

energy = atoms.get_potential_energy()
file1 = paropen('out.energy','w')
file1.write(str(energy))
file1.close()

#vibration mode
vib = Vibrations(atoms)
vib.run()
vib.summary(log='vib.log')
vib_energy = vib.get_energies()

file = paropen('out.vib', 'a')
file.write(str(vib_energy))
file.close()
