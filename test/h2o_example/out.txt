 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
 & This job was run on not implemented
 & This job was run on host: not implemented
 & ============================================================ &
 &                                                              &
 &  Welcome to:                                                 &
 &                                                              &
 &                          D A C A P O                         &
 &                                                              &
 &  The plane wave - pseudopotential program                    &
 &                                                              &
 &  Version:                                                    &
 &    $Date: 2009-05-21 05:15:21 -0400 (Thu, 21 May 2009) $     &
 &    $Rev: 472 $                                               &
 &                                                              &
 & ============================================================ &
 &                                                              &
 &   Recent contributors:                                       &
 &                                                              &
 &   2002-  J. Rossmeisl   (Electrostatic Decoupling)           &
 &   1999-  A. Christensen (Fortran90 modularization,           &
 &                          netCDF interface )                  &
 &   1999-  T. Bligaard    (Fortran90)                          &
 &   1996   Y. Morikawa    (Constrained dynamics)               &
 &   1996   A.C.E.Madsen   (MD min)                             &
 &   1996-  L.B.Hansen     (core corr.)                         &
 &   1996   L.Bengtsson    (fast selfconsis. occ.s,             &
 &                          power expansion method)             &
 &   1995   J.J.Mortensen  (selfconsis. GGA)                    &
 &   1995-  O.H.Nielsen    (parallellization + opt.)            &
 &   1990-  B.Hammer                                            &
 &                                                              &
 &
 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  
  
 FILE_IO: Reading netCDF formatted input from file: out.nc
 FILE_IO: Writing netCDF formatted output to file out.nc
 FILE_IO: Attempting to use same file for netCDF input/output
 FILE_IO: Stopfilename: stop
  
 Structure: lattice scaling factor =   1.00000000
 Structure:-----------------------------------------------------------
 Structure: unit cell A            (lattice vectors coloumnwise) :
 Structure:             A1           A2            A3
 Structure:  x    15.00000000    0.00000000    0.00000000 Angstroem
 Structure:  y     0.00000000   15.10000000    0.00000000 Angstroem
 Structure:  z     0.00000000    0.00000000   15.20000000 Angstroem
 Structure:-----------------------------------------------------------
 Structure: recipical unit cell B  (vectors coloumnwise, A B^t = 2 Pi)
 Structure:             B1           B2            B3
 Structure: kx     0.41887902    0.00000000    0.00000000 1/Angstroem
 Structure: ky     0.00000000    0.41610499    0.00000000 1/Angstroem
 Structure: kz     0.00000000    0.00000000    0.41336745 1/Angstroem
 Structure:-----------------------------------------------------------
 Structure: Number of atoms (nions)           :     3
 Structure: Number of different atoms (nspec) :     2
 Structure: Internal atom mapping: -> ALL INFO IN THIS FILE REFERS TO THIS ATOM ORDER <-
 Structure:   atom#  <->  (species, number)
 Structure:     1              1      1
 Structure:     2              1      2
 Structure:     3              2      1

 Structure:-------------------------------------------------------------------------
 Structure:  >>         Ionic positions in scaled coordinates (u,v,w)             <<
 Structure:  atom#   Z        u            v            w         motion constraints
 Structure:-------------------------------------------------------------------------
 Structure:    1     1    0.50010085   0.55118190   0.48033203   1.000  1.000  1.000
 Structure:    2     1    0.50010469   0.44884172   0.48096513   1.000  1.000  1.000
 Structure:    3     8    0.50003251   0.50000234   0.51976395   1.000  1.000  1.000
 Structure:-------------------------------------------------------------------------
 Structure:-------------------------------------------------------------------------
 Structure:  >>         Ionic positions/velocities in cartesian coordinates       <<
 Structure:  atom#   Z        x            y            z          initial velocity 
 Structure:                           [Angstroem]                  [Angstroem/fs]   
 Structure:-------------------------------------------------------------------------
 Structure:    1     1    7.50151275   8.32284675   7.30104691   0.000  0.000  0.000
 Structure:    2     1    7.50157033   6.77750994   7.31066994   0.000  0.000  0.000
 Structure:    3     8    7.50048763   7.55003536   7.90041200   0.000  0.000  0.000
 Structure:-------------------------------------------------------------------------
 Structure: 
 Structure: Minimum distance between any two atoms (Angstroem) :  0.972
 Structure: Mimimum distance found between atom  2 and atom  3 (for box repetition   0  0  0)
 Structure: 

 Structure: no spherical constrained pair was found
  
  
 tmp_read: ------------------------- input summary -------------------------
 tmp_read: nspin  =    1 (spin polarized calc = 2)
 tmp_read: nbands =   10 (bands in calculation)
 tmp_read: iscxc  =    6 [ XC Functional = RPBE                          GGA]
 tmp_read: nsymax =    6 (maximum number of symmetries applied)
 tmp_read: idebug =    0 (debugging level)
 tmp_read: ------------------------------------------------------------------


 PAD: ecut (wave function cutoff) =     340.1500 eV
 FFT: Double grid used
 PAD: ecut (wave function cutoff) =     340.1500 eV
 PAD: ecut (dense grid cutoff)    =     500.0000 eV
 PAD: minimum grid enclosing the sphere G^2 < 4*E_cut(WaweFct):  92x 92x 92
 PAD: Number of G-vectors (ngdens) below 4*ecut :       392343
 PAD: minimum grid enclosing the sphere G^2 < 4*E_cut(Density): 110x112x112
 PAD: Number of G-vectors (ngdens) below 4*ecut :       699265
 FFT: netCDF softgrid_dim found
 FFT: ngx =  96 ngy =  96 ngz =  96
 FFT: netCDF hardgrid_dim found
 FFT: ngxhard = 110 ngyhard = 112 ngzhard = 112
 SYM: There are            8  point operations in the lattice point group
 SYM: There are            1  point operations in the space group
 SYM: The identity:                 1
 SYM: Number of 2-fold operations:  0
 SYM: Needed are            0  generators:
 SYM: The inversion is not contained in the space group
 SYM: The k-point set will be reduced via the time inversion symmetry
 SYM: 
 KPT:            1  BZ kpoints specified in file
 KPT: k-points in the irriducible Brillouin zone (nkprun) :    1
 KPT: ----------------------------------------------------------
 KPT: k-point      k-point in units of          k-point
 KPT: number      B1        B2       B3         weigth 
 KPT: ----------------------------------------------------------
 KPT:    1    0.000000  0.000000  0.000000    1.000000
 KPT: ----------------------------------------------------------
 KPT: 
 KPT: Chadi-Cohen asymptotic error estimate:  0.001982150517
 KPT: (see PRB 8, 5747 (1973); 13, 5188 (1976))
  
 KPT: nkpmem :            1
 PAD:
 PAD: Nominal # of PW  Average # of PW   Max # of PW
 PAD: for this cutoff  in k-point set    in  k-point set
 PAD:      49043.5361       49029.0000         49029
 PAD:
 PAD: Plane waves of E_kin below         25.00 Ryd =  340.15 eV accepted
 PAD: Effective E_kin for plane waves    25.00 Ryd =  340.08 eV
 call allocate_van_us_data
 setuop:  
 setuop:  ------ Task / iteration control ---------
 setuop:  
 setuop: Ion dynamics type  :  Static
 setuop: Internal ion dynamics method:  Static
 setuop: ConvergenceControl - niter  =       1000000 (max main loop iterations)
 setuop: ConvergenceControl - uppcpu =  100000000.00 hours (cpu time hard limit)
 setuop: ConvergenceControl - criteria for breaking electronic minimization:
 setuop: ConvergenceControl - Absolute energy convergence =  0.10D-04
 setuop: ConvergenceControl - Absolute density convergence =  0.10D-03
 setuop: ConvergenceControl - Absolute occupation convergence =  0.10D-02
 setuop: ConvergenceControl - Repeated convergences =     1
 setuop: ConvergenceControl - Max absolute force change   =      0.100000 eV/A
 setuop: ConvergenceControl - Max relative force change   =      0.050000  
 setuop: Using eigsolve to diag. the hamiltonian
 setuop: Occupation statistics type = FermiDirac
 setuop: FermiTemperature =      0.010000  eV
 setuop: ndiapb =             2 (diagonalizations per band)
 setuop: dipole correction: off
 setuop:  
 setuop:  ------ printout setup ---------
 setuop:  
 setuop: Print spatial quantity:  None
 setuop: Electronic work function not printed
 setuop: Atom projected DOS not printed
 setuop:  
 setuop:  ------ atomic characteristics -------
 setuop:  

 setuop: ----------------------------------------------------------------
 setuop: species  chemical  friction  dynamic mass
 setuop: number    symbol    [0..1]       [u]     
 setuop: ----------------------------------------------------------------
 setuop:    1         H     0.500000    3.000000
 setuop:    2         O     0.500000    3.000000
 setuop: ----------------------------------------------------------------
 PSP: pseudo-potential file for specie            1 :/gpfs/runtime/opt/dacapo-mva/2.7.16-mva2.0/PSP/ch_e9g4.pseudo

    ============================================================
    |  PSEUDOPOTENTIAL REPORT FOR ATOMIC SPECIES:  1           |
    |        pseudo potential version   7   0   0              |
    ------------------------------------------------------------
    |  hydrogen             Perdew Wang 1991    EXCHANGE-CORR  |
    |  Z(nuclear) =   1.    Z(valence)\( 1\) =   1.              |
    |  Non linear core correction included:  no                |
    |  Core radius non lin core corr.(RPCOR) =   0.00000   a.u.|
    |  ATOMIC ENERGY =   -0.920271770 Ry                       |
    |  Self consistent all electron atomic config:             |
    |  INDEX    ORBITAL      OCCUPATION      ENERGY(Ry)        |
    |    1        100           1.00      -0.481067649         |
    |  Radii for conservation of augmentation charge moments:  |
    |  RINNER(a.u.) =   0.9000
    |  (see Phys Rev B 47 10142 (1993), Eq.28)                 |
    |  NEW GENERATION SCHEME:                                  |
    |  Partial wave set used to generate projectors:           |
    |  Number of radial partial waves (NBETA) =      2         |
    |  Number of radial gridpts per wave (KKBETA) =    403     |
    |  Pseudiz. radius for the local pspot (RCLOC) =  0.87 a.u.|
    |  Partial wave set for generating the pseudopot:          |
    |    IBETA   L        EPSILON(Ry)   RCUT(a.u.)             |
    |      1     0      -0.481063112     0.90                  |
    |      2     0       0.900000000     0.90                  |
    ============================================================
 PSP:                       Valence-Z            1.0000
 PSP:                       Core-alpha           0.7722
 PSP: pseudo-potential file for specie            2 :/gpfs/runtime/opt/dacapo-mva/2.7.16-mva2.0/PSP/co_gef_e13_gga.pseudo

    ============================================================
    |  PSEUDOPOTENTIAL REPORT FOR ATOMIC SPECIES:  2           |
    |        pseudo potential version   7   0   0              |
    ------------------------------------------------------------
    |  oxygen               Perdew Wang 1991    EXCHANGE-CORR  |
    |  Z(nuclear) =   8.    Z(valence)\( 2\) =   6.              |
    |  Non linear core correction included:  no                |
    |  Core radius non lin core corr.(RPCOR) =   0.00000   a.u.|
    |  ATOMIC ENERGY =  -31.634296294 Ry                       |
    |  Self consistent all electron atomic config:             |
    |  INDEX    ORBITAL      OCCUPATION      ENERGY(Ry)        |
    |    1        200           2.00      -1.760964278         |
    |    2        210           4.00      -0.670166655         |
    |  Radii for conservation of augmentation charge moments:  |
    |  RINNER(a.u.) =   0.7000  0.7000  0.7000
    |  (see Phys Rev B 47 10142 (1993), Eq.28)                 |
    |  NEW GENERATION SCHEME:                                  |
    |  Partial wave set used to generate projectors:           |
    |  Number of radial partial waves (NBETA) =      4         |
    |  Number of radial gridpts per wave (KKBETA) =    519     |
    |  Pseudiz. radius for the local pspot (RCLOC) =  1.00 a.u.|
    |  Partial wave set for generating the pseudopot:          |
    |    IBETA   L        EPSILON(Ry)   RCUT(a.u.)             |
    |      1     0      -1.760964691     1.30                  |
    |      2     0      -0.670170483     1.30                  |
    |      3     1      -1.760964691     1.30                  |
    |      4     1      -0.670170483     1.30                  |
    ============================================================
 Number of different projectors :           10
 Number of projectors :           12
 PSP:                       Valence-Z            6.0000
 PSP:                       Core-alpha          16.2053
 setuop: Unit cell is charge neutral
 setuop: Unit cell contains:     8.000000 electrons
 setuop:   
 setuop: netCDF output control
 setuop: WaveFunction is not written to NetCDF file
 setuop: ChargeDensity is not writen to NetCDF file
 setuop: Eff. Potential not writen to NetCDF file
 setuop: Electrostatic potential not writen to NetCDF file
 setuop: Stress tensor is not written to NetCDF file
 setuop: ----------------------------------------------------------------
 WFG: wavefunction array type = complex*16

 density_mixing: Updating charge using Pulay mixing
 density_mixing: ChargeMixing method : 
 density_mixing: Pulay               
 density_mixing: noldrd =   10 (charge mixing history)
 density_mixing: denmix =      0.100000 (Pulay mixing coefficient)
 density_mixing: GvectorFactor     1.000000
 density_mixing: Using linear mixing (No Kerker preconditioning)
 density_mixing: Using linear mixing (No Kerker preconditioning)
 DAM    87442       0.000401       0.307594      63.421391
 WFG: could not read wave-function from netCDF set 
 WFG: assumes this is a new calculation
 lkpnew =  T
 FFTW : Setup grid 1         110         112         112
 WFG:            6 atomic wavefunctions used to initialize wavefunctions
 WFG: Setup initial potential
 FFTW : Setup grid 2          96          96          96
 WFG: Start loop over atoms
 WFG: init wavefunction nsp =            1 natm =            1
 WFG: init wavefunction nsp =            1 natm =            2
 WFG: init wavefunction nsp =            2 natm =            1
 lwork could be improved          12          11
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION    1 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 damden: setup new d-matrix
 TR2   -326.522934      0.000000   -427.086175   -458.440309     31.780608      0.000000    100.095522      0.041245      0.000000   -427.086175    427.086175    458.440309   -109.006096      0.000000   -326.522934   -326.522934      0.000000
 EIGSLV: Eigsolve was called with the following parameter:
 EIGSLV:     lgenev:          T
 EIGSLV:     n:                     49029
 EIGSLV:     m:                        10
 EIGSLV:     ldpsi:                 49029
 EIGSLV:     blocksize:                10
 EIGSLV:     maxiter:                   2
 EIGSLV:     max subsp. dim:           20
 EIGSLV:     tolerance:        1.00000000000000005E-004
 TR0      E(free)      E(kin)       E(pot)       E(hac)       E(xcc)       E(vnl)       E(ewa)       E(alp)       E(eig)       E(ext)       E(zero)      E(tot)       E(dipc)
 EXC     Exc_pz       Exc_gga      Exc_vwn      Exc_pbe    Exc_revPBE     Exc_RPBE
 TR1   -471.729376    219.419402   -427.086175   -458.440309     31.780608      0.000000    100.095522      0.041245   -145.206442      0.000000   -471.729376   -471.729376      0.000000
 EXC   -105.961435   -108.801271   -105.999078   -108.282274   -108.870503   -109.006096
 TOT:  CPU time                  Total energy
 TOT:                   LDA            GGA            LDA
 TOT:             PerdewZunger   PerdewWang91      VosWilNus
 TOT:  CPU time   non-selfcons   non-selfcons   non-selfcons
 TOT:   seconds          eV             eV             eV
 DFT:  CPU time                           Total energy
 DFT:                 LDA            GGA-II           PBE         revPBE          RPBE98       
 DFT:             VoskoWilkNus   PerdewWang91   PerdewBurkeE   PBE_kap=1.245  PBE_0.804exp   
 DFT:  CPU time   non-selfcons   non-selfcons   non-selfcons   non-selfcons      selfcons
 DFT:   seconds          eV             eV             eV             eV            eV   
 TOT:      58.9      -468.6847      -471.5246      -468.7224
 DFT:      58.9      -468.7224      -471.5246      -471.0056      -471.5938      -471.7294
 FOR:  Ion    F_x      F_y      F_z
 FOR:   #     eV/A     eV/A     eV/A
 FOR:   1   0.0111168   8.9959106  -6.5774402
 FOR:   2   0.0123173  -9.3683099  -6.7520388
 FOR:   3   0.0366678   0.2995281 -20.8103395
 convergence:    
 convergence:                Change in
 convergence:       Density       Occup.     Energy    |F|      dF/F    |Abs. force|
 convergence:      0.000000     0.000000  -471.729375713   0.000   0.000  26.281
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION    2 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   0    2996.452116
 damden: setup new d-matrix
 TR1   -470.457673    215.949422   -426.593172   -466.244601     32.516959      0.000000    100.095522      0.041245   -136.866799      0.000000   -470.457673   -470.457673      0.000000
 EXC   -107.685756   -108.801271   -105.999078   -108.282274   -108.870503   -111.146756
 TOT:      65.4      -466.9967      -468.1122      -465.3100
 DFT:      65.4      -465.3100      -468.1122      -467.5932      -468.1814      -470.4577
 FOR:   1   0.0151665  12.1170726  -8.8859781
 FOR:   2   0.0166886 -12.5547619  -9.0724561
 FOR:   3  -0.0735388   0.7987817  41.4642965
 convergence:   2996.452116     0.000000     1.271702928   0.000   0.000  46.751
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION    3 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   1    1754.408366
 damden: setup new d-matrix
 TR1   -469.582345    203.908569   -420.142198   -487.831060     33.504818      0.000000    100.095522      0.041245   -115.392871      0.000000   -469.582345   -469.582345      0.000000
 EXC   -111.146683   -108.801271   -105.999078   -108.282274   -108.870503   -114.613829
 TOT:      70.6      -466.1152      -463.7698      -460.9676
 DFT:      70.6      -460.9676      -463.7698      -463.2508      -463.8390      -469.5823
 FOR:   1   0.0151665  12.1170726  -8.8859781
 FOR:   2   0.0166886 -12.5547619  -9.0724561
 FOR:   3  -0.0735388   0.7987817  41.4642965
 convergence:   1754.408366     0.000000     0.875327407   0.000   0.000  46.751
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION    4 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   2     311.848440
 damden: setup new d-matrix
 TR1   -469.645040    204.926309   -418.851427   -486.435389     33.373820      0.000000    100.095522      0.041245   -116.720238      0.000000   -469.645040   -469.645040      0.000000
 EXC   -110.816605   -108.801271   -105.999078   -108.282274   -108.870503   -114.231247
 TOT:      75.6      -466.2304      -464.2151      -461.4129
 DFT:      75.6      -461.4129      -464.2151      -463.6961      -464.2843      -469.6450
 FOR:   1   0.0151665  12.1170726  -8.8859781
 FOR:   2   0.0166886 -12.5547619  -9.0724561
 FOR:   3  -0.0735388   0.7987817  41.4642965
 convergence:    311.848440     0.000000    -0.062694180   0.000   0.000  46.751
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION    5 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   3     176.971033
 damden: setup new d-matrix
 TR1   -469.735260    203.950855   -412.735144   -492.560429     33.586332      0.000000    100.095522      0.041245   -110.897931      0.000000   -469.735260   -469.735260      0.000000
 EXC   -111.718637   -108.801271   -105.999078   -108.282274   -108.870503   -115.067329
 TOT:      80.5      -466.3866      -463.4692      -460.6670
 DFT:      80.5      -460.6670      -463.4692      -462.9502      -463.5384      -469.7353
 FOR:   1   0.0151665  12.1170726  -8.8859781
 FOR:   2   0.0166886 -12.5547619  -9.0724561
 FOR:   3  -0.0735388   0.7987817  41.4642965
 convergence:    176.971033     0.000000    -0.090220600   0.000   0.000  46.751
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION    6 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   4      28.290438
 damden: setup new d-matrix
 TR1   -469.728385    201.782416   -412.202531   -495.524381     33.744326      0.000000    100.095522      0.041245   -108.085097      0.000000   -469.728385   -469.728385      0.000000
 EXC   -112.226060   -108.801271   -105.999078   -108.282274   -108.870503   -115.592447
 TOT:      85.6      -466.3620      -462.9372      -460.1350
 DFT:      85.6      -460.1350      -462.9372      -462.4182      -463.0064      -469.7284
 FOR:   1   0.0151665  12.1170726  -8.8859781
 FOR:   2   0.0166886 -12.5547619  -9.0724561
 FOR:   3  -0.0735388   0.7987817  41.4642965
 convergence:     28.290438     0.000000     0.006875042   0.000   0.000  46.751
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION    7 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   5       3.356285
 damden: setup new d-matrix
 TR1   -469.730181    201.050787   -411.475420   -496.876763     33.827396      0.000000    100.095522      0.041245   -106.817581      0.000000   -469.730181   -469.730181      0.000000
 EXC   -112.473846   -108.801271   -105.999078   -108.282274   -108.870503   -115.859304
 TOT:      90.7      -466.3447      -462.6721      -459.8700
 DFT:      90.7      -459.8700      -462.6721      -462.1532      -462.7414      -469.7302
 FOR:   1   0.0151665  12.1170726  -8.8859781
 FOR:   2   0.0166886 -12.5547619  -9.0724561
 FOR:   3  -0.0735388   0.7987817  41.4642965
 convergence:      3.356285     0.000000    -0.001795917   0.000   0.000  46.751
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION    8 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   6      13.069915
 damden: setup new d-matrix
 TR1   -469.726967    201.811400   -412.278014   -495.048143     33.714655      0.000000    100.095522      0.041245   -108.530246      0.000000   -469.726967   -469.726967      0.000000
 EXC   -112.135689   -108.801271   -105.999078   -108.282274   -108.870503   -115.501089
 TOT:      95.8      -466.3616      -463.0271      -460.2250
 DFT:      95.8      -460.2250      -463.0271      -462.5082      -463.0964      -469.7270
 FOR:   1   0.0151665  12.1170726  -8.8859781
 FOR:   2   0.0166886 -12.5547619  -9.0724561
 FOR:   3  -0.0735388   0.7987817  41.4642965
 convergence:     13.069915     0.000000     0.003214398   0.000   0.000  46.751
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION    9 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   7       1.899014
 damden: setup new d-matrix
 TR1   -469.727533    202.117869   -412.427295   -494.596684     33.688515      0.000000    100.095522      0.041245   -108.956131      0.000000   -469.727533   -469.727533      0.000000
 EXC   -112.056019   -108.801271   -105.999078   -108.282274   -108.870503   -115.416750
 TOT:     100.8      -466.3668      -463.1121      -460.3099
 DFT:     100.8      -460.3099      -463.1121      -462.5931      -463.1813      -469.7275
 FOR:   1   0.0151665  12.1170726  -8.8859781
 FOR:   2   0.0166886 -12.5547619  -9.0724561
 FOR:   3  -0.0735388   0.7987817  41.4642965
 convergence:      1.899014     0.000000    -0.000566606   0.000   0.000  46.751
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION   10 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   8       0.272140
 damden: setup new d-matrix
 TR1   -469.728215    202.252685   -412.547983   -494.365133     33.674866      0.000000    100.095522      0.041245   -109.174716      0.000000   -469.728215   -469.728215      0.000000
 EXC   -112.015531   -108.801271   -105.999078   -108.282274   -108.870503   -115.374511
 TOT:     106.6      -466.3692      -463.1550      -460.3528
 DFT:     106.6      -460.3528      -463.1550      -462.6360      -463.2242      -469.7282
 FOR:   1   0.0115154   9.0954708  -6.7403149
 FOR:   2   0.0127840  -9.4760424  -6.9196002
 FOR:   3   0.0369284   0.4697748 -21.3425244
 convergence:      0.272140     0.000000    -0.000682147   0.000   0.000  26.862
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION   11 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   9       0.022747
 damden: setup new d-matrix
 TR1   -469.728383    202.289589   -412.606694   -494.288574     33.670214      0.000000    100.095522      0.041245   -109.246790      0.000000   -469.728383   -469.728383      0.000000
 EXC   -112.002001   -115.111628   -112.039724   -114.559538   -115.213402   -115.360485
 TOT:     114.4      -466.3699      -469.4795      -466.4076
 DFT:     114.4      -466.4076      -469.4795      -468.9274      -469.5813      -469.7284
 FOR:   1  -0.0001453  -0.0312751  -0.0065701
 FOR:   2   0.0001839  -0.2345591  -0.2103611
 FOR:   3  -0.0003323   0.2660701   0.2244411
 convergence:      0.022747     0.000000    -0.000167766   0.000   0.000   0.471
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION   12 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   9       0.002347
 damden: setup new d-matrix
 TR1   -469.728391    202.295193   -412.610006   -494.283823     33.669916      0.000000    100.095522      0.041245   -109.251252      0.000000   -469.728391   -469.728391      0.000000
 EXC   -112.001073   -115.110648   -112.038786   -114.558547   -115.212391   -115.359473
 TOT:     123.7      -466.3700      -469.4796      -466.4077
 DFT:     123.7      -466.4077      -469.4796      -468.9275      -469.5813      -469.7284
 FOR:   1  -0.0001393  -0.0293910  -0.0068010
 FOR:   2   0.0002012  -0.2364704  -0.2105790
 FOR:   3  -0.0002609   0.2661123   0.2279232
 convergence:      0.002347     0.000000    -0.000008114   0.000   0.000   0.473
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION   13 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   9       0.001653
 damden: setup new d-matrix
 TR1   -469.728393    202.310918   -412.619697   -494.258328     33.668541      0.000000    100.095522      0.041245   -109.275374      0.000000   -469.728393   -469.728393      0.000000
 EXC   -111.996763   -115.106241   -112.034466   -114.554153   -115.207982   -115.355071
 TOT:     133.1      -466.3701      -469.4796      -466.4078
 DFT:     133.1      -466.4078      -469.4796      -468.9275      -469.5813      -469.7284
 FOR:   1  -0.0001238  -0.0277545  -0.0084552
 FOR:   2   0.0002307  -0.2382129  -0.2122271
 FOR:   3  -0.0004659   0.2658803   0.2206570
 convergence:      0.001653     0.000000    -0.000002109   0.000   0.000   0.471
------------------------------------------------------------------
>>>>>>>>>>>>>>>>>>>> MAIN LOOP ITERATION   14 <<<<<<<<<<<<<<<<<<<<
------------------------------------------------------------------
 DAM:   9       0.000062
 damden: setup new d-matrix
 TR1   -469.728393    202.311374   -412.620281   -494.259101     33.668594      0.000000    100.095522      0.041245   -109.274654      0.000000   -469.728393   -469.728393      0.000000
 EXC   -111.996937   -115.106439   -112.034639   -114.554350   -115.208187   -115.355276
 TOT:     142.4      -466.3701      -469.4796      -466.4078
 DFT:     142.4      -466.4078      -469.4796      -468.9275      -469.5813      -469.7284
 FOR:   1  -0.0001198  -0.0283170  -0.0082342
 FOR:   2   0.0002363  -0.2376657  -0.2119999
 FOR:   3  -0.0003865   0.2658603   0.2194655
 TOT:      stopping    0.000000
 convergence:      0.000062     0.000000    -0.000000054   0.000   0.000   0.470
 ANALYSIS PART OF CODE
 EIG   Nb  Nkpt   Eigen value      Occupation 
 EIG   1   1    -22.8264667742    2.0000000000
 EIG   2   1    -10.6567217487    2.0000000000
 EIG   3   1     -6.7889938824    2.0000000000
 EIG   4   1     -4.7651446252    2.0000000000
 EIG   5   1      1.3643270245    0.0000000000
 EIG   6   1      2.3066734328    0.0000000000
 EIG   7   1      2.6325763359    0.0000000000
 EIG   8   1      2.7703686897    0.0000000000
 EIG   9   1      2.8335309455    0.0000000000
 EIG  10   1      2.8466935752    0.0000000000
 Wannier: kpoints in direction n            1           1
 Wannier: kpoints in direction n            2           1
 Wannier: kpoints in direction n            3           1
 TIM: Number of iterations =           14
 TIM: Seconds User:   146.9 System:     0.0 U+S:   146.9
 TIM: Setup        31.1%       45.7 CPU seconds (   3.265 sec/iter)
 TIM: damden       46.2%       67.8 CPU seconds (   4.846 sec/iter)
 TIM: localF        5.4%        7.9 CPU seconds (   0.564 sec/iter)
 TIM: nsc energy   15.1%       22.2 CPU seconds (   1.583 sec/iter)
 TIM: Analysis      2.0%        2.9 CPU seconds (   0.206 sec/iter)
 TIM: libtos        0.3%        0.4 CPU seconds (   0.030 sec/iter)
 TIM:             ------  ---------------------
 TIM: Sum         100.0%      146.9 CPU seconds (  10.494 sec/iter)
 TIM:             ======  =====================
 TIM:
 TIM: Timing of some individual parts
 TIM: nonlocF       3.4%        5.0 CPU seconds (   0.354 sec/iter)
 TIM:   -vkbloop    0.1%        0.2 CPU seconds (   0.011 sec/iter)
 TIM:   -newdd      3.3%        4.8 CPU seconds (   0.342 sec/iter)
 TIM: Ewald         0.0%        0.0 CPU seconds (   0.001 sec/iter)
 TIM: cal_bec       0.4%        0.5 CPU seconds (   0.037 sec/iter)
 TIM: -loop1        0.2%        0.2 CPU seconds (   0.017 sec/iter)
 TIM: V_NL          0.4%        0.6 CPU seconds (   0.043 sec/iter)
 TIM: V_NL1         0.4%        0.6 CPU seconds (   0.042 sec/iter)
 TIM: V_NL2         0.2%        0.3 CPU seconds (   0.018 sec/iter)
 TIM:
 TIM: Timing of individual parts of damden
 TIM: damden       34.1%       50.1 CPU seconds (   3.575 sec/iter)
 TIM: -addusdens    6.9%       10.1 CPU seconds (   0.721 sec/iter)
 TIM: -nonlocF      3.4%        5.0 CPU seconds (   0.354 sec/iter)
 TIM: -newd         7.3%       10.8 CPU seconds (   0.769 sec/iter)
 TIM: --ddot        0.2%        0.4 CPU seconds (   0.026 sec/iter)
 TIM: -tidyup       9.4%       13.8 CPU seconds (   0.987 sec/iter)
 TIM: H_diagonal   12.1%       17.8 CPU seconds (   1.271 sec/iter)
 TIM: -apply_H     11.0%       16.2 CPU seconds (   1.154 sec/iter)
 TIM:  -vnlwav      1.1%        1.6 CPU seconds (   0.113 sec/iter)
 TIM:  -wf_FFT      8.5%       12.6 CPU seconds (   0.897 sec/iter)
 TIM: -Updat_wf     0.2%        0.3 CPU seconds (   0.021 sec/iter)
 TIM: -diag_HEEV    0.0%        0.0 CPU seconds (   0.000 sec/iter)
 TIM: -BLAS_ops     1.2%        1.8 CPU seconds (   0.130 sec/iter)
 TIM: -Restart      0.4%        0.6 CPU seconds (   0.045 sec/iter)
 TIM: -Eig. proj    0.0%        0.0 CPU seconds (   0.001 sec/iter)
 TIM: -Residual     0.1%        0.2 CPU seconds (   0.014 sec/iter)
 TIM: Subroutine apply_H was called    43 times (   0.376 sec/call)
 TIM: -Form_BHB     0.2%        0.3 CPU seconds (   0.022 sec/iter)
 TIM: -vec_rotat    0.4%        0.5 CPU seconds (   0.038 sec/iter)
 TIM: -residual     0.1%        0.2 CPU seconds (   0.014 sec/iter)
 clexit: exiting the program
